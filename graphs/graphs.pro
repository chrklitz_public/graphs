TEMPLATE = app
CONFIG += c++11
CONFIG -= qt
CONFIG += console
CONFIG -= debug_and_release debug_and_release_target

DEFINES += GRAPHS_LIB
DEFINES += QT_DEPRECATED_WARNINGS

include(boost_include.pri)


SOURCES +=

HEADERS +=

test {
  include(test/testsources.pri)
} else {
  SOURCES += graphs/main.cpp
}
